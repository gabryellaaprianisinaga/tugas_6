SELECT * FROM `customers` WHERE 1;


# Jumlah komplain setiap bulan
SELECT MONTH(date_received) AS bulan, COUNT(*) AS jumlah_komplain
FROM customers
GROUP BY MONTH(date_received);

# Komplain yang memiliki tags ‘Older American’
SELECT * from customers where tags= 'Older American';


# Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah
CREATE VIEW complaint_viewsssss AS
SELECT company, count(case when company_response_to_consumer='Closed' then 1 end) AS 'Closed' , count(case when company_response_to_consumer='Closed with explanation' then 1 end) as 'Closed with explanation' , count(case when company_response_to_consumer='Closed with non-monetary relief' then 1 end) as 'Closed with non-monetary relief'
FROM customers where company = 'Wells Fargo & Company'
GROUP BY company ;

SELECT * from complaint_viewsssss;